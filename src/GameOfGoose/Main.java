package GameOfGoose;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import GameOfGoose.model.SimpleModel;
import GameOfGoose.view.start.StartPresenter;
import GameOfGoose.view.start.StartView;



public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        SimpleModel model = new SimpleModel();
        StartView view = new StartView();
        StartPresenter startPresenter = new StartPresenter(model,view);
        Scene scene = new Scene(view);
        primaryStage.setScene(scene);
        startPresenter.addWindowEventHandlers();
        primaryStage.setTitle("GameOfGoose");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
